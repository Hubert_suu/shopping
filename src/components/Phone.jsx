import React from "react";
import background from "./images/background.png";
import iphone11_white from "./images/iphone11-white.jpg";
import iphonex_black from "./images/iphonex-black.jpg";
import iphone11pro_gray from "./images/iphone11pro-gray.jpg";
function Phone(){
    return(
        <div className="phone">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-3 filter-menu"> 
                        <div className="row">
                            <div className="col-12 filter-title"> Home  | Phone</div>
                        </div>

                        <div className="row ">
                               <div className="col-12 categories" ><b>Brand</b></div>
                        </div>

                        <div className="row">
                            <div className="col-2"></div>
                            <div className="col-8">
                                <div className="input-group">
                                    <select class=" filter" id="inputGroupSelect02">
                                        <option selected>select brand</option>
                                        <option value="1">Apple</option>
                                        <option value="2">OnePlus</option>
                                        <option value="3">Sansung</option>
                                        <option value="4">Oppo</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div className="row ">
                               <div className="col-12 categories" ><b>Model</b></div>
                        </div>

                        <div className="row">
                            <div className="col-2"></div>
                            <div className="col-8">
                                <div className="input-group">
                                    <select class=" filter font-control" id="inputGroupSelect02">
                                        <option selected>search model</option>
                                        <option value="1">iphone 7</option>
                                        <option value="2">iphone 8</option>
                                        <option value="3">iphone x</option>
                                        <option value="4">iphone 11</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div className="row ">
                               <div className="col-12 categories" ><b>Price Range (Kyat)</b></div>
                        </div>

                        <div className="row">
                            <div className="col-3"></div>
                            <div className="col-3 range-text">Min</div>
                            <div className="col-3 range-text">Max</div>
                         </div>

                         <div className="row">
                            <div className="col-3"></div>
                            <div className="col-4"><input type="number" className="range" min="0" max="9999999" placeholder="00.00"/></div>
                            <div className="col-4"><input type="number" className="range" min="1" max="9999999" placeholder="00.00"/></div>
                         </div>

                         <div className="row ">
                               <div className="col-12 categories" ><b>Select Condition</b></div>
                        </div>

                        <div className="row">
                            <div className="col-3"></div>
                            <div className="col-3"><input type="radio" id="used" name="condition" value="used" className="condition"/>
                                <label for="male" className="condition-text">Used</label>
                            </div>
                            <div className="col-3"><input type="radio" id="new" name="condition" value="new" className="condition"/>
                                <label for="male"  className="condition-text">New</label>
                            </div>
                        </div>
                        
                        <div className="row ">
                               <div className="col-12 categories" ><b>Region</b></div>
                        </div>

                        <div className="row">
                            <div className="col-2"></div>
                            <div className="col-8">
                                <div className="input-group">
                                    <select class=" filter font-control" id="inputGroupSelect02">
                                        <option selected>Search Region</option>
                                        <option value="1">Yangon</option>
                                        <option value="2">Mandalay</option>
                                        <option value="3">Shan state</option>
                                        <option value="4">Ka chin state</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div className="row ">
                               <div className="col-12 categories" ><b>Township</b></div>
                        </div>

                        <div className="row">
                            <div className="col-2"></div>
                            <div className="col-8">
                                <div className="input-group">
                                    <select class=" filter font-control" id="inputGroupSelect02">
                                        <option selected>search Township</option>
                                        <option value="1">Mayangone</option>
                                        <option value="2">MayNigone</option>
                                        <option value="3">Thingangyun</option>
                                        <option value="4">Hla Thar</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div> 
{/* --------------------------------product list------------------------------------------------ */}
                    <div className="col-8 ">
                            <div className="row background-row">
                                <div className="col-12">
                                    <img src={background} className="background"/>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col"></div>
                            </div>
{/* ------------------------------------product -------------------------------------------------- */}
                            <div className="row product-row">
                                <div className="col-4">
                                    <div class="card">
                                        <img class="card-img-top" src={iphone11_white} alt="Card image"/>
                                        <div class="card-body">
                                        <h4 class="card-title">iphone11</h4>
                                        <p class="card-text">128G white</p>
                                        <a href="#" class="btn btn-primary">View Detail</a>
                                    </div>
                                 </div>
                                </div>

                                <div className="col-4">
                                    <div class="card">
                                        <img class="card-img-top" src={iphonex_black} alt="Card image"/>
                                        <div class="card-body">
                                        <h4 class="card-title">iphoneX</h4>
                                        <p class="card-text">128G Black</p>
                                        <a href="#" class="btn btn-primary">View Detail</a>
                                    </div>
                                 </div>
                                </div>

                                <div className="col-4">
                                    <div class="card">
                                        <img class="card-img-top" src={iphone11pro_gray} alt="Card image"/>
                                        <div class="card-body">
                                        <h4 class="card-title">iphone7Plus</h4>
                                        <p class="card-text">256G Red</p>
                                        <a href="#" class="btn btn-primary">View Detail</a>
                                    </div>
                                 </div>
                                </div>
                            </div>

                            <div className="row product-row">
                                <div className="col-4">
                                    <div class="card">
                                        <img class="card-img-top" src={iphone11_white} alt="Card image"/>
                                        <div class="card-body">
                                        <h4 class="card-title">iphone11</h4>
                                        <p class="card-text">128G white</p>
                                        <a href="#" class="btn btn-primary">View Detail</a>
                                    </div>
                                 </div>
                                </div>

                                <div className="col-4">
                                    <div class="card">
                                        <img class="card-img-top" src={iphonex_black} alt="Card image"/>
                                        <div class="card-body">
                                        <h4 class="card-title">iphoneX</h4>
                                        <p class="card-text">128G Black</p>
                                        <a href="#" class="btn btn-primary">View Detail</a>
                                    </div>
                                 </div>
                                </div>

                                <div className="col-4">
                                    <div class="card">
                                        <img class="card-img-top" src={iphone11pro_gray} alt="Card image"/>
                                        <div class="card-body">
                                        <h4 class="card-title">iphone7Plus</h4>
                                        <p class="card-text">256G Red</p>
                                        <a href="#" class="btn btn-primary">View Detail</a>
                                    </div>
                                 </div>
                                </div>
                            </div>

                            <div className="row product-row">
                                <div className="col-4">
                                    <div class="card">
                                        <img class="card-img-top" src={iphone11_white} alt="Card image"/>
                                        <div class="card-body">
                                        <h4 class="card-title">iphone11</h4>
                                        <p class="card-text">128G white</p>
                                        <a href="#" class="btn btn-primary">View Detail</a>
                                    </div>
                                 </div>
                                </div>

                                <div className="col-4">
                                    <div class="card">
                                        <img class="card-img-top" src={iphonex_black} alt="Card image"/>
                                        <div class="card-body">
                                        <h4 class="card-title">iphoneX</h4>
                                        <p class="card-text">128G Black</p>
                                        <a href="#" class="btn btn-primary">View Detail</a>
                                    </div>
                                 </div>
                                </div>

                                <div className="col-4">
                                    <div class="card">
                                        <img class="card-img-top" src={iphone11pro_gray} alt="Card image"/>
                                        <div class="card-body">
                                        <h4 class="card-title">iphone7Plus</h4>
                                        <p class="card-text">256G Red</p>
                                        <a href="#" class="btn btn-primary">View Detail</a>
                                    </div>
                                 </div>
                                </div>
                            </div>
  {/* ---------------------------Pagination---------------------------------- */}

                            <div className="row">
                                <div className="col-3"></div> 

                                <div className="col-4">
                                <nav aria-label="...">
  <ul class="pagination">
    <li class="page-item disabled">
      <a class="page-link" href="#" tabindex="-1">Previous</a>
    </li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item active">
      <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
    </li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
      <a class="page-link" href="#">Next</a>
    </li>
  </ul>
</nav>
                                </div>
                            </div>
                           
                    </div> 

          
                    

                </div>
            </div>
        </div>
    );
}
export default Phone;