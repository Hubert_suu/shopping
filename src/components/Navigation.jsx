import React from "react";
import { Link, withRouter } from "react-router-dom";
import "./style.css";
import 'bootstrap/dist/js/bootstrap.bundle.min';
import logo from "./images/logo.png";
import background from "./images/background.png";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


function Navigation(props) {
  return (
    <div className="navigation  fixed-top">
        <div class="container-fluid">
            <div class="row">
                <div class=" con1 col-xl-12 col-md-12 col-sm-12">
                          <div class="row bar  " id="navi-bar">
                              <nav class=" col-12 navbar navbar-expand-md navbar-light navbar-fnt-white navbar-style">
                                      <div class=" col-12 collapse navbar-collapse" id="navbarNav">

                                            <ul class=" col-12 navbar-nav">
                                                <li class="col-2 nav-item"> <img src={logo} className="logo"/>  </li>

                                                <li class="col-1 nav-item active">            
                                                    <div class={` ${
                                                        props.location.pathname === "/{}" ? "active" : ""
                                                        }`}>  
                                                        <Link class="nav-link" to="/">
                                                        
                                                        <span class="sr-only">(current)</span>
                                                        
                                                        Phones
                                                        </Link>
                                                    </div> 
                                                </li>

                                                    
                                                <li class="col-1 nav-item">
                                                        <div class={` ${
                                                            props.location.pathname === "/Grape" ? "active" : ""
                                                            }`}>  
                                                            <Link class="nav-link" to="/Grape">
                                                
                                                                Tablets
                                                            </Link>
                                                        </div> 
                                                    
                                                </li>
                                                    
                                    

                                                
                                                <li class="col-1 nav-item">
                                                        <div class={` ${
                                                            props.location.pathname === "/Grape" ? "active" : ""
                                                            }`}>  
                                                            <Link class="nav-link" to="/Grape">
                                                
                                                                Watches
                                                            </Link>
                                                        </div> 
                                                    
                                                </li>

                                                
                                                <li class="col-1 nav-item">
                                                        <div class={` ${
                                                            props.location.pathname === "/PineApple" ? "active" : ""
                                                            }`}>  
                                                            <Link class="nav-link" to="/PineApple">
                                                
                                                                Accessories
                                                            </Link>
                                                        </div> 
                                                </li>

                                                <li class="col-1 nav-item">
                                                        <div class={` ${
                                                            props.location.pathname === "/PineApple" ? "active" : ""
                                                            }`}>  
                                                            <Link class="nav-link" to="/PineApple">
                                                
                                                                Stores
                                                            </Link>
                                                        </div> 
                                                </li>
                                                
                                                <li className="col-2 nav-item">
                                                    <form class="form-inline d-flex justify-content-center md-form form-sm mt-0">
                                                        <i class="fas fa-search" aria-hidden="true"></i>
                                                        <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search"
                                                            aria-label="Search" />
                                                    </form>
                                                    
                                                </li>
                                                
                                                <li className="col-1 nav-item">    <FontAwesomeIcon icon={'fas' , 'user'} class="icon-style"/></li>
                                        </ul>
                                      </div>
                                </nav>
                            
                      </div>
                      <div className="row "> 
                          <div Class="col-6">  </div>
                    </div>
                </div>
          </div>
           
           
        </div>
    </div>
  );
}

export default withRouter(Navigation);