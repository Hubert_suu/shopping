import React from "react";
import "./style.css";
import facebook from "./images/facebook.png";
import instagram from "./images/instagram.png";

function Footer(){
        return(
            <div className="footer">
                <div className="container "> 
                        <div className="row ml-3">
                            <div className="col-3">
                                <div className="row footer-title"><b>Fonelawka.com</b></div>
                                <div className="row foot-text"> Sell your phone in the largest</div>
                                <div className="row foot-text"> make place in the Myanmar.</div>
                                <div className="row foot-text">
                                    <div className="col-1">  <img src={facebook}  className="facebook-icon"/></div>
                                    <div className="col-1">  <img src={instagram} className="instagram-icon"/></div>
                               </div>
                             </div>

                             <div className="col-3">
                                <div className="row footer-title"><b> Browse by Category</b></div>
                                <div className="row foot-text">Phones </div>
                                <div className="row foot-text">Tablets</div>
                                <div className="row foot-text">Watches</div>
                                <div className="row foot-text">Accessories</div>
                                <div className="row foot-text">Stores</div>
                             </div>

                             <div className="col-3">
                                <div className="row footer-title"><b>More info</b></div>
                                <div className="row foot-text">How it work </div>
                                <div className="row foot-text">About us</div>
                                <div className="row foot-text">Decline rules</div>
                                <div className="row foot-text">Terms & Condition</div>
                             </div>

                             <div className="col-3">
                                <div className="row footer-title"><b> Customer Care</b></div>
                                <div className="row foot-text">FAQ </div>
                                <div className="row foot-text">Terms of services</div>
                                <div className="row foot-text">Privacy Policy</div>
                                <div className="row foot-text">Contact us</div>
                             </div>
                        </div>
                             <hr/>
                        <div className="row">
                            <div className="col-12 copyright">
                            Copyright© 2020 Sonic Lab All Rights Reserved
                            </div>
                        </div>
                </div>
            </div>
        );
}
export default Footer;