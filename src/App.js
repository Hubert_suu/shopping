import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.js';
import {Navigation,Footer,Phone} from "./components";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
function App() {
  return (
    <div className="App">
        <Router>
             <Navigation />
             <Phone />
            
        </Router>
       <Footer/>
    </div>
  );
}

export default App;
