// import the library
import { library } from '@fortawesome/fontawesome-svg-core';

// import your icons

import {faUser} from '@fortawesome/free-solid-svg-icons';

library.add(
  faUser
  // more icons go here
);
